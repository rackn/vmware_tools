import pytest

from unittest.mock import patch, MagicMock

from drpy import action_runner
from drpy.exceptions import DRPException
from drpy.models.job import JobAction
from drpy.models.job import Job
from drpy.models.runner import AgentState
from drpy.api.client import Client
from drpy.models.machine import Machine



# Be sure the first item in this list is an empty
# string
testdata = [
    "",
    12345,
    {"a": 1},
    [1, 2, 3]
]


@pytest.mark.parametrize("job_action", testdata)
def test_run_command_called_with_wrong_types_raises_valueerror(job_action):
    """
    Testing to make sure we raise a value error when called and not using
    a valid JobAction object.
    """
    with pytest.raises(ValueError):
        action_runner.run_command(job_action)


def test_run_command_raises_drpexception_when_ja_contains_path():
    """
    Run command should only work when a Path is not set. If a Path
    is set then you should be using add_file and placing Contents
    at Path.
    """
    ja = JobAction(Path="/foo/bar/baz")
    with pytest.raises(DRPException) as dre:
        action_runner.run_command(ja)
    assert str(dre.value) == "run_command called when path provided."


@pytest.mark.parametrize("job_action", testdata)
def test_add_file_called_with_wrong_types_raises_valueerror(job_action):
    """
    Testing to make sure we raise a value error when called not using
    a valid JobAction
    """
    with pytest.raises(ValueError):
        action_runner.add_file(job_action)


@pytest.mark.parametrize("job_action", testdata[1:])
def test_add_file_called_with_wrong_types_in_path_raise_exception(job_action):
    """
    Testing to make sure if Path is not a str then we raise ValueError
    """
    ja = JobAction(Path=job_action)
    with pytest.raises(ValueError):
        action_runner.add_file(ja)


def test_run_command_stderr_to_stdout():

    # Define the mock output for stdout and stderr
    mock_stdout = b"Standard output message\n"
    mock_stderr = b"Error message\n"
    combined_output = mock_stdout + mock_stderr  # Simulate combined output when stderr -> stdout

    # Create a MagicMock to mock `subprocess.run`
    mock_run = MagicMock()
    mock_run.stdout = combined_output  # Combine stdout and stderr as expected
    mock_run.returncode = 0  # Simulate a successful command execution
    content = "echo 'hello world'"
    ja = JobAction(Name="test_stdout", Path='', Content=content)
    client = Client(endpoint="https://localhost:8092/api/v3")
    client.token = "token"
    job = Job(Token="foo", Uuid="some uuid")
    machine = Machine(Uuid="some machine uuid")
    agt_st = AgentState(client=client, job=job, machine=machine)

    with patch("subprocess.run", return_value=mock_run):
        # Call the function being tested
        result = action_runner.run_command(job_action=ja, timeout=30, expath="/tmp/rackn", agent_state=agt_st)

        # Assertions
        assert result["Exit_Code"] == 0  # Ensure the exit code is correct
        assert result["Out"] == combined_output  # Ensure the output includes both stdout and stderr
        assert b"Error message" in result["Out"]  # Confirm stderr content is included in the output
        assert b"Standard output message" in result["Out"]  # Confirm stdout content is present
