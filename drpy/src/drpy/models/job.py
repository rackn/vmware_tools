from .base import Namespace


class Job(Namespace):

    attrs = [
        'Archived',
        'Available',
        'BootEnv',
        'Bundle',
        'Current',
        'CurrentIndex',
        'EndTime',
        'Endpoint',
        'Errors',
        'ExitState',
        'Machine',
        'Meta',
        'NextIndex',
        'Previous',
        'ReadOnly',
        'Stage',
        'StartTime',
        'State',
        'Task',
        'Token',
        'Uuid',
        'Validated',
        'Workflow'
    ]


class JobAction(Namespace):

    attrs = [
        'Content',
        'Meta',
        'Name',
        'Path'
    ]
